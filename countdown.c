///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
// make
// coutdown
// 
// Example Output:
//    Years: 1  Days: 14  Hours: 21  Minutes: 39  Seconds: 29
//    Years: 1  Days: 14  Hours: 21  Minutes: 39  Seconds: 30
//    Years: 1  Days: 14  Hours: 21  Minutes: 39  Seconds: 31
//    Years: 1  Days: 14  Hours: 21  Minutes: 39  Seconds: 32
//    Years: 1  Days: 14  Hours: 21  Minutes: 39  Seconds: 33
//    Years: 1  Days: 14  Hours: 21  Minutes: 39  Seconds: 34
//    Years: 1  Days: 14  Hours: 21  Minutes: 39  Seconds: 35
  
//
// @author Oze Farris <ofarris@gmail.com>
// @date   21_02_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

#define YEAR_S 31556952
#define DAY_S 86400
#define HOUR_S 3600
#define MIN_S 60

int main() {


   time_t now;

   int years, days, hours, mins, secs, count;

   struct tm start;
   start = *localtime(&now);

   start.tm_sec = 0;
   start.tm_min = 0;
   start.tm_hour = 12;
   start.tm_mday = 7;
   start.tm_mon = 1;
   start.tm_year = 121;
 while(true){

      time(&now);
      mktime(&start);

      count = difftime(now, mktime(&start));

      years = count / YEAR_S;
      days = count % YEAR_S / DAY_S;
      hours = count % DAY_S / HOUR_S;
      mins = count % HOUR_S / MIN_S;
      secs = count % MIN_S;
      
      printf("Years: %i  Days: %i  Hours: %i  Minutes: %i  Seconds: %i\n", years, days, hours, mins, secs);
      sleep(1);
   }

   return 0;
}

















